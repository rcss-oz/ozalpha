# ozAlpha

ozAlpha is a project that provide RoboCup soccer simulation client creating environment in JavaScript employing librcsc.

Overview :
User <-> JavaScript <-> JavaScript ozAlpha APIs <-> JS/C++ embedding tool <-> librcsc

Future works :
Users can make simple rcss clients in JavaScript using JS-librcsc (librcsc.js) interface.
 librcsc.js has WorldModel, Primitive rscc2d command qeue, Basic behaviors, etc. 

Samples : simple clients, ML clients, OZ-RP client, and so on.